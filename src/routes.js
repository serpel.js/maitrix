import React from "react";

import { Icon } from "@chakra-ui/react";
import {
  MdPerson,
} from "react-icons/md";

import Presale from "./views/presale";
import Home from "./views/home";
import NFTMarket from "./views/nftmarket";

const routes = [
  {
    name: "Presale",
    layout: "/admin",
    path: "/presale",
    icon: <Icon as={MdPerson} width='20px' height='20px' color='inherit' />,
    component: Presale,
  },
  { 
    name: "NFT Market",
    layout: "/admin",
    path: "/nftmarket",
    component: NFTMarket,
  },
  { 
    name: "Home",
    layout: "/admin",
    path: "/",
    component: Home,
  },
];

export default routes;
