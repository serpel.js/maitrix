import React from "react";
import ReactDOM from "react-dom";
import "assets/css/App.css";
import { HashRouter, Route, Switch, Redirect } from "react-router-dom";
import AdminLayout from "layouts/admin";
import { ChakraProvider } from "@chakra-ui/react";
import theme from "theme/theme";
import { BrowserRouter } from "react-router-dom";
import { Web3ReactProvider } from "@web3-react/core";
import { ethers } from "ethers";

const getLibrary = (provider) => {
  const library = new ethers.providers.Web3Provider(provider);
  library.pollingInterval = 8000; //process.env.WALLET_PULLING_FRECUENCY frequency provider is polling
  return library;
};

ReactDOM.render(
  <ChakraProvider theme={theme}>
    <Web3ReactProvider getLibrary={getLibrary}>
      <BrowserRouter>
        <React.StrictMode>
          <HashRouter>
            <Switch>
              <Route path={`/`} component={AdminLayout} />
              <Redirect from='/' to='/admin' />
            </Switch>
          </HashRouter>
        </React.StrictMode>
      </BrowserRouter>
    </Web3ReactProvider>
  </ChakraProvider>,
  document.getElementById("root")
);
