

// Chakra imports
import {
    Box,
    Flex,
    Progress,
    Text,
    useColorModeValue,
    Stack,
    Icon
} from "@chakra-ui/react";
// Custom components
import Card from "components/card/Card.js";
import React from "react";
import IconBox from "components/icons/IconBox";

import {
    MdSavings
  } from "react-icons/md";


export default function Default(props) {

    const brandColor = useColorModeValue("brand.500", "white");
    const boxBg = useColorModeValue("secondaryGray.300", "whiteAlpha.100");
    
    const { used, total } = props;
    // Chakra Color Mode
    const textColorSecondary = "gray.400";

    return (
        <Card py='15px'>
            <Stack direction="row" width="auto" spacing={4}>
                <Flex width="auto">
                    <IconBox
                        w='56px'
                        h='56px'
                        bg={boxBg}
                        spacing={6}
                        icon={
                            <Icon w='32px' h='32px' as={MdSavings} color={brandColor} />
                        }
                    />
                </Flex>
                <Box w='100%' mt='auto'>
                    <Flex w='100%' justify='space-between' mb='10px'>
                        <Text color={textColorSecondary} fontSize='sm' maxW='40%'>
                            {used} Claimed
                        </Text>
                        <Text color={textColorSecondary} fontSize='sm' maxW='40%'>
                            {total} Total
                        </Text>
                    </Flex>
                    <Progress
                        align='start'
                        colorScheme='brandScheme'
                        value={(used / total) * 100}
                        w='100%'
                    />
                </Box>
            </Stack>
        </Card>
    );
}
