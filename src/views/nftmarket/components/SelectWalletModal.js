import {
    VStack,
    HStack,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalBody,
    ModalCloseButton,
    Button,
    Text
  } from "@chakra-ui/react";
  import { Image } from "@chakra-ui/react";
  import { useWeb3React } from "@web3-react/core";
  import { connectors } from "connectors";
  import Metamask from "assets/img/wallets/metamask.png"
  import WalletConnect from "assets/img/wallets/wallet-connect.png"
  import CoinbaseWallet from "assets/img/wallets/coinbase-wallet.png"
  
  export default function SelectWalletModal({ isOpen, closeModal, setProvider }) {
    const { activate } = useWeb3React();
  
    const defineProvider = (type) => {
      window.localStorage.setItem("provider", type);
      setProvider(type);
    };
  
    return (
      <Modal isOpen={isOpen} onClose={closeModal} isCentered>
        <ModalOverlay />
        <ModalContent w="300px">
          <ModalHeader>Select Wallet</ModalHeader>
          <ModalCloseButton
            _focus={{
              boxShadow: "none"
            }}
          />
          <ModalBody paddingBottom="1.5rem">
            <VStack>
              <Button
                variant="outline"
                onClick={() => {
                  activate(connectors.coinbaseWallet);
                  defineProvider("coinbaseWallet");
                  closeModal();
                }}
                w="100%"
              >
                <HStack w="100%" justifyContent="center">
                  <Image
                    src={CoinbaseWallet}
                    alt="Coinbase Wallet Logo"
                    width={25}
                    height={25}
                    borderRadius="3px"
                  />
                  <Text>Coinbase Wallet</Text>
                </HStack>
              </Button>
              <Button
                variant="outline"
                onClick={() => {
                  activate(connectors.walletConnect);
                  defineProvider("walletConnect");
                  closeModal();
                }}
                w="100%"
              >
                <HStack w="100%" justifyContent="center">
                  <Image
                    src={WalletConnect}
                    alt="Wallet Connect Logo"
                    width={26}
                    height={26}
                    borderRadius="3px"
                  />
                  <Text>Wallet Connect</Text>
                </HStack>
              </Button>
              <Button
                variant="outline"
                onClick={() => {
                  activate(connectors.injected);
                  defineProvider("injected");
                  closeModal(); 
                }}
                w="100%"
              >
                <HStack w="100%" justifyContent="center">
                  <Image
                    src={Metamask}
                    alt="Metamask Logo"
                    width={25}
                    height={25}
                    borderRadius="3px"
                  />
                  <Text>Metamask</Text>
                </HStack>
              </Button>
            </VStack>
          </ModalBody>
        </ModalContent>
      </Modal>
    );
  }