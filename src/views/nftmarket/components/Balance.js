// Chakra imports
import {
  Box,
  Flex,
  Progress,
  Text,
  useColorModeValue,
  Button,
  ButtonGroup
} from "@chakra-ui/react";
// Custom components
import Card from "components/card/Card.js";
import React from "react";

export default function Balance(props) {
  const { used, total, balance, randomNumber, account, active, canClaim } = props;
  // Chakra Color Mode
  const textColorPrimary = useColorModeValue("secondaryGray.900", "white");
  const textColorSecondary = "gray.400";
  return (
    <Card mb={{ base: "0px", lg: "20px" }} align='center'>
      <Flex minWidth='max-content' alignItems='right' gap='2' >
        {!active && canClaim ? (   
          <Button colorScheme='teal' variant='solid'>Claim</Button>      
        ) : null}
      </Flex>
      {active ? (
        <>
          <Text color={textColorPrimary} fontSize='6xl' mt='10px'>
          {balance}
          </Text>
          <Text color={textColorPrimary} fontWeight='bold' fontSize='xl' mt='4px'>
            Your Balance, {account}
          </Text>
        </>
        ) :
        <>
          <Text color={textColorPrimary} fontSize='6xl' mt='10px'>
            {randomNumber}
          </Text>
          <Text color={textColorPrimary} fontWeight='bold' fontSize='xl' mt='4px'>
            This could be your feature profit
          </Text>
        </>}
      <Text
        color={textColorSecondary}
        fontSize='md'
        maxW={{ base: "100%", xl: "80%", "3xl": "60%" }}
        mx='auto'>
        still <b>{Math.round((((100-(used/total)*100) * 100) + Number.EPSILON) * 100) / 10000}% </b> tokens available, hurry!
      </Text>
      <Box w='100%' mt='auto'>
        <Flex w='100%' justify='space-between' mb='10px'>
          <Text color={textColorSecondary} fontSize='sm' maxW='40%'>
            {used} MAI Reserved
          </Text>
          <Text color={textColorSecondary} fontSize='sm' maxW='40%'>
            {total} MAI Available
          </Text>
        </Flex>
        <Progress
          align='start'
          colorScheme='brandScheme'
          value={(used / total) * 100}
          w='100%'
        />
      </Box>
    </Card>
  );
}
