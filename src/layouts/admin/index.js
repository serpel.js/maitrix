// Chakra imports
import { Portal, Box, useDisclosure } from "@chakra-ui/react";
import Footer from "components/footer/FooterAdmin.js";
// Layout components
import NavbarSimpleLinks from "components/navbar/NavbarSimpleLinks";
import Navbar from "components/navbar/NavbarAdmin.js";
import { SidebarContext } from "contexts/SidebarContext";
import React, { useState } from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import routes from "routes.js";

// Custom Chakra theme
export default function Dashboard(props) {
  const { ...rest } = props;
  // states and functions
  const [fixed] = useState(false);
  const [toggleSidebar, setToggleSidebar] = useState(false);
  // functions for changing the states from components
  const getRoutes = (routes) => {
    return routes.map((prop, key) => {
      if (prop.layout === "/admin") {
        return (
          <Route
            path={prop.path}
            component={prop.component}
            key={key.toString()}
          />
        );
      }
      if (prop.collapse) {
        return getRoutes(prop.itsems);
      }
      if (prop.category) {
        return getRoutes(prop.items);
      } else {
        return null;
      }
    });
  };
  document.documentElement.dir = "ltr";
  const { onOpen } = useDisclosure();
  return (
    <Box>
      <SidebarContext.Provider
        value={{
          toggleSidebar,
          setToggleSidebar,
        }}>
        <Box>
          <Portal>
            <Box
              top={{ base: "0px", md: "0px", xl: "0px" }}
              position="absolute"
              w={{
                base: "calc(100vw - 4%)",
                md: "calc(100vw - 3%)",
                lg: "calc(100vw - 4%)",
                xl: "calc(100vw - 4%)",
                "2xl": "calc(100vw - 1%)",
              }}>
              <NavbarSimpleLinks
                brandText={"LOGO"}
                currentLanguage="English"
                {...rest}
              />
            </Box>
          </Portal>
          <Box
            mx='auto'
            p={{ base: "20px", md: "30px" }}
            pe='20px'
            minH='100vh'
            pt='50px'>
            <Switch>
              {getRoutes(routes)}
            </Switch>
          </Box>
          <Box>
            <Footer />
          </Box>
        </Box>
      </SidebarContext.Provider>
    </Box>
  );
}
